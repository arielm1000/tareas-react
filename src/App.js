import React from 'react';
import Tareas from './componentes/Tareas';
import './App.css';

function App() {
  return (
    <div className="App">
        <Tareas/>
    </div>
  );
}

export default App;
