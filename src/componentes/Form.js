import React, { useState } from "react";
import { v4 as uuidv4 } from 'uuid';

function Form({guardarTarea, tareasStorage}) {
    const [tarea,modificarTarea] = useState({id: uuidv4(), nombre: ''});
    const {nombre} = tarea;

    const changeState = (e) => {
        console.log(e.target.name, e.target.value);
        modificarTarea({
            ...tarea,
            [e.target.name]: e.target.value
        });
    }
 
    const enterTarea = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            //controles de campo
            if(tarea.nombre === "")
            {
                alert("Todos los Campos son requeridos")
                return;
            }
            console.log("Todos los campos estan completos");
            //guardar tarea state
            guardarTarea(tarea);
            //guardo la tarea en el local storage
            tareasStorage.push(tarea);
            localStorage.setItem('TareasLocal',JSON.stringify(tareasStorage));

            //restaurar formulario
            modificarTarea({
                id: uuidv4(),
                nombre: ''
            })
        }
    }
    return(
    <form className="text-left">
        <div className="alert alert-warning d-flex justify-content-center" role="alert">
            <h3>Ingresa tus Tareas</h3>
        </div>
        <input 
            className="form-control my-3" 
            type="text" 
            name="nombre" 
            id="nombre" 
            placeholder="Ingrese su Tarea..."
            onChange={changeState}
            onKeyPress={enterTarea} 
            value={nombre}
        />
     </form>);
}

export default Form;