import React from  'react';

function MostrarTareas({tarea, tareas, modificarTareas, tareasStorage}) {
    const borrarTarea = id => {
//        if(confirm("¿Borra?")){
            //Borra en el state
            modificarTareas(tareas.filter( tarea => tarea.id !== id));
            //Borra en el local storege
            let indice = tareasStorage.findIndex(tareasbus => tareasbus.id === tareas.id);
            console.log(indice);
            console.log(tareasStorage[indice]);
            tareasStorage.splice(indice,1);
            localStorage.setItem('TareasLocal',JSON.stringify(tareasStorage));
//        }   
    }
    return(
        <tr className="alert alert-warning">
        <td className = "text-left">{tarea.nombre}</td>
        <td>
            <button 
                type="button" 
                className="btn btn-danger btn-sm"
                onClick={() => borrarTarea(tarea.id)}
            ><i className="far fa-trash-alt"></i></button></td>
        </tr>
    )
}

export default MostrarTareas;