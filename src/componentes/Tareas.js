import React, {useState, Fragment} from 'react';
import Form from './Form'
import MostrarTareas from './MostrarTareas';


function Tareas(){
/*     class TareasLocal{            
        constructor(id, nombre){
           this.id = id;
           this.nombre = nombre;
       }
 
       set setId(id){
           this.id = id;
       }
   
       get getId(){
           return this.id;
       }
   
       set setNombre(nombre){
           this.nombre = nombre;
       }
   
       get getNombre(){
           return this.nombre;
       }
   } */
    //leo el local storage
    let tareasStorage = JSON.parse(localStorage.getItem('TareasLocal'));
    if(!tareasStorage){
        tareasStorage = [];
    }
    console.log("local storage" + tareasStorage);

    //const [tareas,modificarTareas] = useState([]);
    const [tareas,modificarTareas] = useState(tareasStorage);

    const guardarTarea = tarea => { 
        modificarTareas([...tareas, tarea]);
    }

    return(
        <Fragment>
        <div className ="container alert alert-success w-50 mt-3">
            <div className="d-flex alert alert alert-light m-4 justify-content-center" role="alert">
                <h1>Bienvenido</h1>
            </div>
            <div className="container mt-12">
                <div className="row justify-content-center">
                    <div className="col-10">
                        <Form
                            guardarTarea = {guardarTarea}
                            tareasStorage = {tareasStorage}
                        />
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10">
                    <div className="table-responsive-sm">
                        <table className="table">
                        <thead className="thead-dark">
                            <tr>
                            <th scope="col" className="col-7">Tarea</th>
                            <th scope="col" className="col-3">Borrar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                tareas.map(tarea => (
                                    <MostrarTareas 
                                        key={tarea.id}
                                        tarea={tarea}
                                        tareas={tareas}
                                        modificarTareas = {modificarTareas}
                                        tareasStorage = {tareasStorage}
                                    />
                                ))
                            }
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </Fragment>
    );
}

export default Tareas;